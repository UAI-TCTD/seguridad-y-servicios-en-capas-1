﻿Imports TC.Unidad2.Ejemplo.DAL
Imports TC.Unidad2.Ejemplo.BE

Public Class FamiliaBLL
    Implements ICrud(Of Familia)

    Dim dal As New FamiliaDAL


    Public Function Delete(id As Long) As Boolean Implements ICrud(Of Familia).Delete
        Return dal.Delete(id)
    End Function

    Public Function GetAll() As IQueryable(Of Familia) Implements ICrud(Of Familia).GetAll
        Return dal.GetAll()
    End Function

    Public Function GetById(id As Long) As Familia Implements ICrud(Of Familia).GetById
        Return dal.GetById(id)
    End Function

    Public Function Save(Entity As Familia) As Object Implements ICrud(Of Familia).Save
        dal.Save(Entity)
    End Function
End Class
