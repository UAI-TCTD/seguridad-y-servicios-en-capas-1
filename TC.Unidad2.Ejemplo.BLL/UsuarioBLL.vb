﻿Imports TC.Unidad2.Ejemplo.BE
Imports TC.Unidad2.Ejemplo.Seguridad
Imports TC.Unidad2.Ejemplo.DAL

Public Class UsuarioBLL
    Implements ICrud(Of Usuario)

    Dim dal As New UsuarioDAL

    Public Function Login(username As String, password As String) As LoginResult
        Dim res As LoginResult
        Dim usuario As Usuario

        usuario = dal.GetAll.Where(Function(u) u.Username.Equals(username)).FirstOrDefault

        If IsNothing(usuario) Then
            Throw New LoginException(LoginResult.InvalidUsername)
        Else
            If CryptoManager.Compare(password, usuario.Password) Then
                SessionManager.Instance.Login(usuario)
                Return LoginResult.ValidUser
            Else
                Throw New LoginException(LoginResult.InvalidPassword)
            End If
        End If


        Return res

    End Function

    Public Function Delete(id As Long) As Boolean Implements ICrud(Of Usuario).Delete
        dal.Delete(id)
    End Function

    Public Function GetAll() As IQueryable(Of Usuario) Implements ICrud(Of Usuario).GetAll
        Return dal.GetAll()
    End Function

    Public Function GetById(id As Long) As Usuario Implements ICrud(Of Usuario).GetById
        Return dal.GetById(id)
    End Function

    Public Function Save(Entity As Usuario) As Object Implements ICrud(Of Usuario).Save
        dal.Save(Entity)
    End Function
End Class
