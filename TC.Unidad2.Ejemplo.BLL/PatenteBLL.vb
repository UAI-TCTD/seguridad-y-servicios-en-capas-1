﻿Imports TC.Unidad2.Ejemplo.DAL
Imports TC.Unidad2.Ejemplo.BE

Public Class PatenteBLL
    Implements ICrud(Of Patente)

    Dim dal As New PatenteDAL


    Public Function Delete(id As Long) As Boolean Implements ICrud(Of Patente).Delete
        Return dal.Delete(id)
    End Function

    Public Function GetAll() As IQueryable(Of Patente) Implements ICrud(Of Patente).GetAll
        Return dal.GetAll()
    End Function

    Public Function GetById(id As Long) As Patente Implements ICrud(Of Patente).GetById
        Return dal.GetById(id)
    End Function

    Public Function Save(Entity As Patente) As Object Implements ICrud(Of Patente).Save
        dal.Save(Entity)
    End Function
End Class
