﻿Imports TC.Unidad2.Ejemplo.BLL
Imports TC.Unidad2.Ejemplo.Seguridad

Public Class frmLogin
    Dim bll As New UsuarioBLL
    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        Try

            If bll.Login(Me.txtUsername.Text, Me.txtPassword.Text) = LoginResult.ValidUser Then
                Dim frm As frmMain = Me.MdiParent
                frm.ValidateForm()
                Me.Close()
            End If
        Catch ex As LoginException
            MsgBox(ex.LoginResult.ToString, MsgBoxStyle.Critical, "Error")
        
        Catch ex2 As Exception
            'handle exception
            MsgBox(ex2.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub
End Class