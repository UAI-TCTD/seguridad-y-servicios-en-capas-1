﻿Imports TC.Unidad2.Ejemplo.Seguridad

Public Class frmEncriptador

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles cmdEncriptar.Click
        Me.txtResult.Text = CryptoManager.EncriptadoSimetrico(Me.txtSource.Text)
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles cmdDesencriptar.Click
        Me.txtResult.Text = CryptoManager.DesencriptadoSimetrico(Me.txtSource.Text)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles cmdHash.Click
        Me.txtResult.Text = CryptoManager.Hash(Me.txtSource.Text)
    End Sub
End Class