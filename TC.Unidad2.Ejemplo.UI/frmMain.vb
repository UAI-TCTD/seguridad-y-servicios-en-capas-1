﻿Imports TC.Unidad2.Ejemplo.Seguridad
Imports TC.Unidad2.Ejemplo.BE

Public Class frmMain


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        ValidateForm()
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuLogout.Click
        SessionManager.Instance().Logout()
        ValidateForm()
    End Sub


    Private Sub mnuInicioSesion_Click(sender As Object, e As EventArgs) Handles mnuInicioSesion.Click
        Dim frm As New frmLogin
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Public Sub ValidateForm()
        Me.mnuInicioSesion.Visible = Not SessionManager.Instance.isLogged
        Me.mnuLogout.Visible = SessionManager.Instance.isLogged

        If SessionManager.Instance.isLogged Then
            Me.statusLabel.Text = SessionManager.Instance.User.Username

        Else
            Me.statusLabel.Text = "Inicie Sesión"
        End If


        'valido permisos
        Me.mnuSeguridadPermisos.Enabled = SessionManager.Instance.IsInRole(TipoPermisoPatente.AsignarPermisos)
        Me.mnuAltaUsuarios.Enabled = SessionManager.Instance.IsInRole(TipoPermisoPatente.AltaUsuario)




    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub PermisosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuSeguridadPermisos.Click
        Dim frm As New frmManejadorPermisos
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Private Sub EncriptadorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EncriptadorToolStripMenuItem.Click
        Dim f As New frmEncriptador
        f.MdiParent = Me
        f.Show()
    End Sub
End Class
