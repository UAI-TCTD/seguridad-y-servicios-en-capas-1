﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmManejadorPermisos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboUsuarios = New System.Windows.Forms.ComboBox()
        Me.treePermisos = New System.Windows.Forms.TreeView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboFamilias = New System.Windows.Forms.ComboBox()
        Me.cmdAgregarFamilia = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboPatentes = New System.Windows.Forms.ComboBox()
        Me.cmdAgregarPatente = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Usuario"
        '
        'cboUsuarios
        '
        Me.cboUsuarios.FormattingEnabled = True
        Me.cboUsuarios.Location = New System.Drawing.Point(61, 26)
        Me.cboUsuarios.Name = "cboUsuarios"
        Me.cboUsuarios.Size = New System.Drawing.Size(160, 21)
        Me.cboUsuarios.TabIndex = 1
        '
        'treePermisos
        '
        Me.treePermisos.Location = New System.Drawing.Point(15, 66)
        Me.treePermisos.Name = "treePermisos"
        Me.treePermisos.Size = New System.Drawing.Size(206, 326)
        Me.treePermisos.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboFamilias)
        Me.GroupBox1.Controls.Add(Me.cmdAgregarFamilia)
        Me.GroupBox1.Location = New System.Drawing.Point(247, 153)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(267, 105)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Familia"
        '
        'cboFamilias
        '
        Me.cboFamilias.FormattingEnabled = True
        Me.cboFamilias.Location = New System.Drawing.Point(20, 33)
        Me.cboFamilias.Name = "cboFamilias"
        Me.cboFamilias.Size = New System.Drawing.Size(228, 21)
        Me.cboFamilias.TabIndex = 5
        '
        'cmdAgregarFamilia
        '
        Me.cmdAgregarFamilia.Location = New System.Drawing.Point(98, 71)
        Me.cmdAgregarFamilia.Name = "cmdAgregarFamilia"
        Me.cmdAgregarFamilia.Size = New System.Drawing.Size(75, 23)
        Me.cmdAgregarFamilia.TabIndex = 4
        Me.cmdAgregarFamilia.Text = "Agregar"
        Me.cmdAgregarFamilia.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboPatentes)
        Me.GroupBox2.Controls.Add(Me.cmdAgregarPatente)
        Me.GroupBox2.Location = New System.Drawing.Point(247, 29)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(267, 105)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Patente"
        '
        'cboPatentes
        '
        Me.cboPatentes.FormattingEnabled = True
        Me.cboPatentes.Location = New System.Drawing.Point(20, 33)
        Me.cboPatentes.Name = "cboPatentes"
        Me.cboPatentes.Size = New System.Drawing.Size(228, 21)
        Me.cboPatentes.TabIndex = 5
        '
        'cmdAgregarPatente
        '
        Me.cmdAgregarPatente.Location = New System.Drawing.Point(98, 71)
        Me.cmdAgregarPatente.Name = "cmdAgregarPatente"
        Me.cmdAgregarPatente.Size = New System.Drawing.Size(75, 23)
        Me.cmdAgregarPatente.TabIndex = 4
        Me.cmdAgregarPatente.Text = "Agregar"
        Me.cmdAgregarPatente.UseVisualStyleBackColor = True
        '
        'frmManejadorPermisos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(554, 429)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.treePermisos)
        Me.Controls.Add(Me.cboUsuarios)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmManejadorPermisos"
        Me.Text = "Permisos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboUsuarios As System.Windows.Forms.ComboBox
    Friend WithEvents treePermisos As System.Windows.Forms.TreeView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboFamilias As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAgregarFamilia As System.Windows.Forms.Button
    Private WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboPatentes As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAgregarPatente As System.Windows.Forms.Button
End Class
