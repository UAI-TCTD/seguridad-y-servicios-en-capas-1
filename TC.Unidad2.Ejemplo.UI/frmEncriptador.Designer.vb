﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEncriptador
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdEncriptar = New System.Windows.Forms.Button()
        Me.cmdDesencriptar = New System.Windows.Forms.Button()
        Me.cmdHash = New System.Windows.Forms.Button()
        Me.txtSource = New System.Windows.Forms.TextBox()
        Me.txtResult = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cmdEncriptar
        '
        Me.cmdEncriptar.Location = New System.Drawing.Point(397, 52)
        Me.cmdEncriptar.Name = "cmdEncriptar"
        Me.cmdEncriptar.Size = New System.Drawing.Size(107, 31)
        Me.cmdEncriptar.TabIndex = 0
        Me.cmdEncriptar.Text = "Encriptar"
        Me.cmdEncriptar.UseVisualStyleBackColor = True
        '
        'cmdDesencriptar
        '
        Me.cmdDesencriptar.Location = New System.Drawing.Point(397, 89)
        Me.cmdDesencriptar.Name = "cmdDesencriptar"
        Me.cmdDesencriptar.Size = New System.Drawing.Size(107, 30)
        Me.cmdDesencriptar.TabIndex = 1
        Me.cmdDesencriptar.Text = "Desencriptar"
        Me.cmdDesencriptar.UseVisualStyleBackColor = True
        '
        'cmdHash
        '
        Me.cmdHash.Location = New System.Drawing.Point(397, 125)
        Me.cmdHash.Name = "cmdHash"
        Me.cmdHash.Size = New System.Drawing.Size(107, 30)
        Me.cmdHash.TabIndex = 2
        Me.cmdHash.Text = "Hash"
        Me.cmdHash.UseVisualStyleBackColor = True
        '
        'txtSource
        '
        Me.txtSource.Location = New System.Drawing.Point(31, 58)
        Me.txtSource.Multiline = True
        Me.txtSource.Name = "txtSource"
        Me.txtSource.Size = New System.Drawing.Size(341, 96)
        Me.txtSource.TabIndex = 3
        '
        'txtResult
        '
        Me.txtResult.Location = New System.Drawing.Point(31, 229)
        Me.txtResult.Multiline = True
        Me.txtResult.Name = "txtResult"
        Me.txtResult.Size = New System.Drawing.Size(482, 96)
        Me.txtResult.TabIndex = 4
        '
        'frmEncriptador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(546, 350)
        Me.Controls.Add(Me.txtResult)
        Me.Controls.Add(Me.txtSource)
        Me.Controls.Add(Me.cmdHash)
        Me.Controls.Add(Me.cmdDesencriptar)
        Me.Controls.Add(Me.cmdEncriptar)
        Me.Name = "frmEncriptador"
        Me.Text = "Encripador"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdEncriptar As System.Windows.Forms.Button
    Friend WithEvents cmdDesencriptar As System.Windows.Forms.Button
    Friend WithEvents cmdHash As System.Windows.Forms.Button
    Friend WithEvents txtSource As System.Windows.Forms.TextBox
    Friend WithEvents txtResult As System.Windows.Forms.TextBox
End Class
