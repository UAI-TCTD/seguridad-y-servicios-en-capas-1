﻿Imports TC.Unidad2.Ejemplo.BLL
Imports TC.Unidad2.Ejemplo.BE

Public Class frmManejadorPermisos
    Dim bll As New UsuarioBLL
    Dim bllPatentes As New PatenteBLL
    Dim bllFamilia As New FamiliaBLL

    Private selected As Usuario
    Private selectedNode As TreeNode
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        Me.cboUsuarios.DataSource = bll.GetAll.ToList
        Me.cboPatentes.DataSource = bllPatentes.GetAll.ToList
        Me.cboFamilias.DataSource = bllFamilia.GetAll.ToList

        ' Add any initialization after the InitializeComponent() call.

    End Sub



    Private Sub cboUsuarios_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboUsuarios.SelectedValueChanged
        selected = Me.cboUsuarios.SelectedItem
        mostrarPermisos()
    End Sub

    Private Function crearNodo(p As PermisoComponent) As TreeNode
        Dim tn As New TreeNode
        tn.Text = p.Nombre
        tn.Tag = p
        Return tn
    End Function

    Private Sub mostrarPermisosRecursivo(p As PermisoComponent, tn As TreeNode)
        For Each p1 In p.ObtenerHijos
            Dim ntn As New TreeNode
            ntn = crearNodo(p1)
            tn.Nodes.Add(ntn)
            If p1.ObtenerHijos.Count > 0 Then
                mostrarPermisosRecursivo(p1, ntn)

            End If
        Next


    End Sub

    Private Sub mostrarPermisos()
        Me.treePermisos.Nodes.Clear()
        Dim n As New TreeNode
        n.Text = "Permisos"
        treePermisos.Nodes.Add(n)
        For Each p In selected.Permisos
            Dim tn As New TreeNode
            tn = crearNodo(p)
            n.Nodes.Add(tn)
            If p.ObtenerHijos.Count > 0 Then
                mostrarPermisosRecursivo(p, tn)
            End If
        Next

    End Sub

    Private Sub treePermisos_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles treePermisos.AfterSelect
        selectedNode = e.Node
        Validar()
    End Sub
    Private Sub Validar()
        If Not selectedNode Is Nothing Then
            Me.GroupBox1.Enabled = Not TypeOf selectedNode.Tag Is Patente
            Me.GroupBox2.Enabled = Not TypeOf selectedNode.Tag Is Patente
        End If
    End Sub

    Private Sub cmdAgregarPatente_Click(sender As Object, e As EventArgs) Handles cmdAgregarPatente.Click
        Dim p As PermisoComponent = selectedNode.Tag
        If TypeOf p Is Familia Then
            p.AgregarPermiso(cboPatentes.SelectedItem)

        Else
            If p Is Nothing Then
                Dim tn As New TreeNode
                selected.Permisos.Add(cboPatentes.SelectedItem)


            End If
        End If
        mostrarPermisos()
    End Sub

    Private Sub cmdAgregarFamilia_Click(sender As Object, e As EventArgs) Handles cmdAgregarFamilia.Click
        Dim p As PermisoComponent = selectedNode.Tag
        If TypeOf p Is Familia Then
            p.AgregarPermiso(cboFamilias.SelectedItem)

        Else
            If p Is Nothing Then
                Dim tn As New TreeNode
                selected.Permisos.Add(cboFamilias.SelectedItem)


            End If
        End If
        mostrarPermisos()
    End Sub
End Class