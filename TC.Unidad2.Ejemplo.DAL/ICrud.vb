﻿Public Interface ICrud(Of T)

    Function GetById(id As Long) As T
    Function Save(Entity As T)
    Function GetAll() As IQueryable(Of T)
    Function Delete(id As Long) As Boolean

End Interface
