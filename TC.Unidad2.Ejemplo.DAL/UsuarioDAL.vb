﻿Imports TC.Unidad2.Ejemplo.BE
Imports TC.Unidad2.Ejemplo.Seguridad

Public Class UsuarioDAL
    Implements ICrud(Of Usuario)

    Dim pdal As New PatenteDAL
    Dim fdal As New FamiliaDAL

    Dim todos As IList(Of Usuario)

    Public Sub New()
        EmularDatos()
    End Sub
    Private Sub agregar(username As String, password As String)

        Dim u As New Usuario
        u.Username = username
        u.Password = CryptoManager.Hash(password)
        todos.Add(u)

    End Sub

    

    Private Function EmularDatos() As IQueryable(Of Usuario)

        todos = New List(Of Usuario)
        agregar("admin", "1234")
        agregar("pepe", "parada")
        agregar("roberto", "gomez")





        Dim u As Usuario = GetAll.Where(Function(i) i.Username.Equals("admin")).FirstOrDefault

        
        Dim p As Patente = pdal.GetAll().FirstOrDefault
        u.Permisos.Add(p)
        





    End Function

    Public Function Delete(id As Long) As Boolean Implements ICrud(Of Usuario).Delete
        For Each u In todos
            If (u.Id.Equals(id)) Then
                todos.Remove(u)
            End If

        Next
    End Function

    Public Function GetAll() As IQueryable(Of Usuario) Implements ICrud(Of Usuario).GetAll
        Return todos.AsQueryable
    End Function

    Public Function GetById(id As Long) As Usuario Implements ICrud(Of Usuario).GetById
        Return todos.Where(Function(t) t.Id.Equals(id)).FirstOrDefault
    End Function

    Public Function Save(Entity As Usuario) As Object Implements ICrud(Of Usuario).Save
        todos.Add(Entity)
    End Function
End Class
