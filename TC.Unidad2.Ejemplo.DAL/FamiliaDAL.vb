﻿Imports TC.Unidad2.Ejemplo.BE

Public Class FamiliaDAL
    Implements ICrud(Of Familia)

    

    Dim todos As IList(Of Familia)

    Public Sub New()
        todos = New List(Of Familia)
        EmularDatos()
    End Sub

    Private Function EmularDatos() As IQueryable(Of Usuario)

        Dim f As New Familia
        f.Nombre = "Puede manejar Permisos"
        todos.Add(f)


        f = New Familia
        f.Nombre = "Puede manejar Usuarios"
        todos.Add(f)



    End Function


    Public Function Delete(id As Long) As Boolean Implements ICrud(Of Familia).Delete
        For Each u In todos
            If (u.Id.Equals(id)) Then
                todos.Remove(u)
            End If

        Next
    End Function

    Public Function GetAll() As IQueryable(Of Familia) Implements ICrud(Of Familia).GetAll
        Return todos.AsQueryable
    End Function

    Public Function GetById(id As Long) As Familia Implements ICrud(Of Familia).GetById
        Return todos.Where(Function(t) t.Id.Equals(id)).FirstOrDefault
    End Function

    Public Function Save(Entity As Familia) As Object Implements ICrud(Of Familia).Save
        todos.Add(Entity)
    End Function
End Class
