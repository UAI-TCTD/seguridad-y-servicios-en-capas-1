﻿Imports TC.Unidad2.Ejemplo.BE

Public Class PatenteDAL
    Implements ICrud(Of Patente)

    Dim todos As IList(Of Patente)

    Public Sub New()
        todos = New List(Of Patente)
        EmularDatos()
    End Sub

    Private Function EmularDatos() As IQueryable(Of Patente)

        Dim p As New Patente
        p.Nombre = "Puede asignar Permisos"
        p.Permiso = TipoPermisoPatente.AsignarPermisos
        todos.Add(p)


        p = New Patente
        p.Nombre = "Puede agregar Usuarios"
        p.Permiso = TipoPermisoPatente.AltaUsuario
        todos.Add(p)



    End Function


    Public Function Delete(id As Long) As Boolean Implements ICrud(Of Patente).Delete
        For Each u In todos
            If (u.Id.Equals(id)) Then
                todos.Remove(u)
            End If

        Next
    End Function

    Public Function GetAll() As IQueryable(Of Patente) Implements ICrud(Of Patente).GetAll
        Return todos.AsQueryable
    End Function

    Public Function GetById(id As Long) As Patente Implements ICrud(Of Patente).GetById
        Return todos.Where(Function(t) t.Id.Equals(id)).FirstOrDefault
    End Function

    Public Function Save(Entity As Patente) As Object Implements ICrud(Of Patente).Save
        todos.Add(Entity)
    End Function
End Class
