﻿Imports System.Security.Cryptography
Imports System.Text
Imports System.Configuration
Imports System.IO



Public Class CryptoManager

    Private Shared hashAlgorithm As HashAlgorithm



    Private Shared Function createString(value As Byte()) As String
        Dim sb As New StringBuilder()
        For index = 0 To value.Length - 1
            sb.Append(value(index).ToString("x2"))
        Next

        Return sb.ToString()

    End Function


    Public Shared Function DesencriptadoSimetrico(textoCifrado As String)
        Dim EncryptionKey As String = ConfigurationManager.AppSettings("CryptoKey") 'PARAMETRIZACIÓN
        Dim cipherBytes As Byte() = Convert.FromBase64String(textoCifrado)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                textoCifrado = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return textoCifrado
    End Function

    Public Shared Function EncriptadoSimetrico(textoPlano As String)
        Dim EncryptionKey As String = ConfigurationManager.AppSettings("CryptoKey")
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(textoPlano)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                textoPlano = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return textoPlano

    End Function

    Public Shared Function Hash(value As String) As String
        hashAlgorithm = MD5.Create()
        Dim data As Byte() = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(value))
        Return createString(data)
    End Function




    Public Shared Function Compare(input As String, hashedInput As String) As Boolean

        Dim value = Hash(input)
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase
        Return comparer.Compare(value, hashedInput).Equals(0)

    End Function








End Class

