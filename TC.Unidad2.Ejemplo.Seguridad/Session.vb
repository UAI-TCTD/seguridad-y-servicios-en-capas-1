﻿Imports TC.Unidad2.Ejemplo.BE

Public Class Session
    Private _user As Usuario
    Public ReadOnly Property User As Usuario
        Get
            Return _user
        End Get
    End Property


    Private Function IsInroleRecursivo(p1 As Patente, permiso As TipoPermisoPatente, valid As Boolean)


        For Each p As PermisoComponent In p1.ObtenerHijos

            If TypeOf p Is Patente Then
                If CType(p, Patente).Permiso.Equals(permiso) Then
                    valid = True
                End If
            Else
                valid = IsInroleRecursivo(p, permiso, valid)
            End If
        Next
        Return valid
    End Function

    Public ReadOnly Property IsInRole(permiso As TipoPermisoPatente) As Boolean
        Get

            If IsNothing(_user) Then
                Return False
            Else
                Dim valid As Boolean = False
                For Each p As PermisoComponent In _user.Permisos



                    If TypeOf p Is Patente Then
                        If CType(p, Patente).Permiso.Equals(permiso) Then
                            valid = True
                        End If


                    Else
                        valid = IsInroleRecursivo(p, permiso, valid)
                    End If



                Next
                Return valid
            End If
        End Get
    End Property

    Public ReadOnly Property isLogged As Boolean
        Get
            Return Not IsNothing(User)
        End Get
    End Property

    Public Function Login(usuario As Usuario)
        _user = usuario

    End Function

    Public Sub Logout()
        _user = Nothing

    End Sub

End Class
