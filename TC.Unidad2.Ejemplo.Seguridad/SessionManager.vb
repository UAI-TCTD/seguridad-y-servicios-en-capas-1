﻿Public Class SessionManager
    Private Shared _instance As Session
    Private Shared ReadOnly _lock As Object = New Object()
    Public Shared ReadOnly Property Instance() As Session
        Get
            SyncLock _lock 'exclusion mutua
                If (_instance Is Nothing) Then
                    _instance = New Session()
                End If
            End SyncLock

            Return _instance
        End Get

    End Property
End Class
