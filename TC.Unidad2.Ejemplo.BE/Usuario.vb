﻿Public Class Usuario
    Inherits Entity
    Public Property Username As String
    Public Property Password As String
    Public Property Permisos As List(Of PermisoComponent)

    Public Sub New()
        Permisos = New List(Of PermisoComponent)
    End Sub

    Public Overrides Function ToString() As String
        Return Username
    End Function
End Class
