﻿Public Class Familia
    Inherits PermisoComponent

    Private hijos As List(Of PermisoComponent)

    Public Sub New()
        hijos = New List(Of PermisoComponent)
    End Sub

    Public Overrides Sub AgregarPermiso(permiso As PermisoComponent)
        hijos.Add(permiso)
    End Sub



    Public Overrides Sub QuitarPermiso(permiso As PermisoComponent)
        hijos.Remove(permiso)
    End Sub

    Public Overrides Function ObtenerHijos() As IList(Of PermisoComponent)
        Return hijos
    End Function

    
End Class


