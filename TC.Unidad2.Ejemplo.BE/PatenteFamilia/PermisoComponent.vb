﻿Public MustInherit Class PermisoComponent
    Inherits Entity

    Public Property Nombre As String

    MustOverride Sub AgregarPermiso(permiso As PermisoComponent)
    MustOverride Sub QuitarPermiso(permiso As PermisoComponent)
    MustOverride Function ObtenerHijos() As IList(Of PermisoComponent)
    Public Overrides Function ToString() As String
        Return Me.Nombre
    End Function
End Class
